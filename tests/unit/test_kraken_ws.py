import asyncio
from contextlib import suppress
import json
import logging
import asynctest
import websockets
from aio_kraken_ws.kraken_ws import KrakenWs


sleep = asyncio.sleep
mock_time = asynctest.Mock()
mock_time.return_value = 60
mock_logger = asynctest.Mock()


async def make_me_sleep(time, loop=None):
    await sleep(0.1, loop=loop)


def get_open_port():
    import socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind(("", 0))
    sock.listen(1)
    port = sock.getsockname()[1]
    sock.close()
    return port


class MockServer:
    """ Mock the Kraken server for unit tests. """

    def __init__(self):
        self.msg = []
        self._task = None
        self.server = None
        self._ws = []

    async def start_server(self, host, port):
        """ Running mock server. """
        self.server = await websockets.server.serve(self._read_msg, host, port)

    async def _read_msg(self, ws, path):
        """ Called on first message received by the server. """
        self._ws.append(ws)
        async for message in ws:
            self.msg.append(json.loads(message))

    @property
    def ws(self):
        """ List of websocket connections. """
        if not self._ws:
            raise RuntimeError("No connections")
        return self._ws

    async def send(self, data, ws=0):
        """ Send data from server. """
        await self.ws[ws].send(json.dumps(data))

    async def send_a_candle(self, timestamp, dataset):
        """ Helper to send dummy candle. """
        pair, interval = dataset
        mock_time.return_value = timestamp
        await self.send([42, [str(timestamp), str(timestamp + 60),
                              100, 101, 99, 100, 1, 60, 1],
                         f"ohlc-{interval}",
                         pair])
        await sleep(0.1)
        mock_time.return_value = timestamp + 60
        await self.send([42, [str(timestamp + 60), str(timestamp + 120),
                              100, 101, 99, 100, 1, 60, 1],
                         f"ohlc-{interval}",
                         pair])
        await sleep(0.1)
        return [timestamp, 100, 101, 99, 100, 60]

    async def close(self):
        """ Shutdown. """
        if self._ws:
            await asyncio.gather(*[ws.close() for ws in self.ws])
        if self.server is not None:
            self.server.close()
            await self.server.wait_closed()


class TestKrakenWs(asynctest.TestCase):
    """ Test KrakenWs against mock server. """

    async def setUp(self):
        mock_time.return_value = 60
        self.port = get_open_port()
        self.mock_server = MockServer()
        await self.mock_server.start_server("127.0.0.1", self.port)
        self.callback = asynctest.CoroutineMock()
        self.url = f"ws://127.0.0.1:{self.port}"
        self.kraken = KrakenWs(self.callback,
                               url=self.url,
                               reset_period=1800)
        self.logger = logging.getLogger("aio_kraken_ws.stream")

    async def wait_connected(self):
        while not self.kraken.is_connected:
            await sleep(0.01)

    async def tearDown(self):
        with suppress(RuntimeError):
            await self.kraken.close()
        await self.mock_server.close()

    @asynctest.patch("time.time", mock_time)
    @asynctest.patch("asyncio.sleep", make_me_sleep)
    async def test_callback_on_new_candle(self):
        """ Send data to get a candle. """
        self.kraken.subscribe([("XBT/EUR", 1)])
        await self.kraken.start()
        await self.wait_connected()
        # When
        candle = await self.mock_server.send_a_candle(120, ("XBT/EUR", 1))
        mock_time.return_value = 182
        await sleep(1)
        # Then
        self.callback.assert_awaited_with("XBT/EUR", 1, *candle)

    @asynctest.patch("time.time", mock_time)
    @asynctest.patch("asyncio.sleep", make_me_sleep)
    @asynctest.patch("aio_kraken_ws.stream.logger.warning", asynctest.Mock())
    async def test_callback_too_long_warning(self):
        """ Warn if callback take more than a minute to processed."""
        # Given
        async def increase_time(*args, **kwargs):
            mock_time.return_value += 61

        self.callback.side_effect = increase_time

        self.kraken.subscribe([('XBT/EUR', 1)])
        await self.kraken.start()
        await self.wait_connected()
        # When
        await self.mock_server.send_a_candle(120, ("XBT/EUR", 1))
        # Then
        await sleep(0.2)
        self.logger.warning.assert_called_with("Callback took more than a minute to process.")

    @asynctest.patch("asyncio.sleep", make_me_sleep)
    @asynctest.patch("aio_kraken_ws.stream.logger.warning", asynctest.Mock())
    async def test_system_status_warning(self):
        """ Kraken server publication on system status changes are logged."""
        # Given
        server_payload = {
            "connectionID": 8628615390848610000,
            "event": "systemStatus",
            "status": "maintenance",
            "version": "1.0.0"
        }
        self.kraken.subscribe([('XBT/EUR', 1)])
        await self.kraken.start()
        await self.wait_connected()
        # When
        await self.mock_server.send(server_payload)
        # Then
        await sleep(0.2)
        self.logger.warning.assert_called_once_with(server_payload)

    @asynctest.patch("asyncio.sleep", make_me_sleep)
    async def test_subscribe_to_one_dataset(self):
        # When
        self.kraken.subscribe([('XBT/EUR', 1)])
        await self.kraken.start()
        await self.wait_connected()
        # Then
        self.assertEqual(self.kraken.datasets, [('XBT/EUR', 1)])
        await sleep(0.2)
        self.assertTrue({
            "event": "subscribe",
            "pair": [
                "XBT/EUR"
            ],
            "subscription": {
                "interval": 1,
                "name": "ohlc"
            }
        } in self.mock_server.msg)

    @asynctest.patch("asyncio.sleep", make_me_sleep)
    async def test_subscribe_to_many_datasets(self):
        # When
        datasets = [('XBT/EUR', 1), ('ETH/EUR', 1), ('XBT/EUR', 5)]
        self.kraken.subscribe(datasets)
        await self.kraken.start()
        await self.wait_connected()
        # Then
        self.assertEqual(set(self.kraken.datasets), set(datasets))
        await sleep(0.2)
        self.assertTrue(({
            "event": "subscribe",
            "pair": [
                "XBT/EUR", "ETH/EUR"
            ],
            "subscription": {
                "interval": 1,
                "name": "ohlc"
            }
        } in self.mock_server.msg) or ({
            "event": "subscribe",
            "pair": [
                "ETH/EUR", "XBT/EUR"
            ],
            "subscription": {
                "interval": 1,
                "name": "ohlc"
            }
        } in self.mock_server.msg))
        self.assertTrue({
            "event": "subscribe",
            "pair": [
                "XBT/EUR"
            ],
            "subscription": {
                "interval": 5,
                "name": "ohlc"
            }
        } in self.mock_server.msg)

    @asynctest.patch("asyncio.sleep", make_me_sleep)
    async def test_subscribe_to_more_than_twenty_datasets(self):
        """ Open a new connection if their is more than 20 commandes. """
        # When
        pairs = ["XBT/EUR",
                 "ETH/EUR",
                 "XBT/USD",
                 "ETH/USD",
                 "XTZ/EUR",
                 "XTZ/USD",
                 "XRP/EUR",
                 "XRP/USD"]
        intervals = [1, 5, 15, 30, 60]
        datasets = [(pair, interval) for pair in pairs for interval in intervals]
        self.kraken.subscribe(datasets)
        await self.kraken.start()
        await self.wait_connected()
        self.assertTrue(len(self.kraken.datasets) // 20, 1)
        # Then
        self.assertEqual(set(self.kraken.datasets), set(datasets))
        await sleep(0.2)
        self.assertTrue(len(self.mock_server.ws) == 2)
        # When
        datasets = [(pair, interval) for pair in pairs for interval in [240, 1440]]
        self.kraken.subscribe(datasets)
        self.assertTrue(len(self.kraken.datasets) // 20, 2)
        # Then
        await sleep(0.2)
        self.assertTrue(len(self.mock_server.ws) == 3)

    @asynctest.patch("asyncio.sleep", make_me_sleep)
    async def test_unsubscribe_to_one_dataset(self):
        # Given
        self.kraken.subscribe([('XBT/EUR', 1)])
        await self.kraken.start()
        await self.wait_connected()
        await sleep(0.2)
        # When
        self.kraken.unsubscribe([('XBT/EUR', 1)])
        # Then
        self.assertEqual(self.kraken.datasets, [])
        await sleep(0.2)
        self.assertTrue({
            "event": "unsubscribe",
            "pair": [
                "XBT/EUR"
            ],
            "subscription": {
                "interval": 1,
                "name": "ohlc"
            }
        } in self.mock_server.msg)

    @asynctest.patch("asyncio.sleep", make_me_sleep)
    async def test_unsubscribe_to_many_datasets(self):
        # Given
        self.kraken.subscribe([('XBT/EUR', 1), ('XBT/EUR', 5)])
        await self.kraken.start()
        await self.wait_connected()
        await sleep(0.1)
        self.assertTrue({
            "event": "subscribe",
            "pair": [
                "XBT/EUR"
            ],
            "subscription": {
                "interval": 1,
                "name": "ohlc"
            }
        } in self.mock_server.msg)
        # When
        self.kraken.unsubscribe([('XBT/EUR', 1), ('XBT/EUR', 5)])
        # Then
        self.assertEqual(self.kraken.datasets, [])
        await sleep(0.2)
        self.assertTrue({
            "event": "unsubscribe",
            "pair": [
                "XBT/EUR"
            ],
            "subscription": {
                "interval": 1,
                "name": "ohlc"
            }
        } in self.mock_server.msg)
        self.assertTrue({
            "event": "unsubscribe",
            "pair": [
                "XBT/EUR"
            ],
            "subscription": {
                "interval": 5,
                "name": "ohlc"
            }
        } in self.mock_server.msg)
        self.assertFalse(self.kraken.is_connected)

    @asynctest.patch("asyncio.sleep", make_me_sleep)
    async def test_server_close_connection(self):
        """ Subscriptions are recovered after a connection close. """
        # Given
        self.kraken.subscribe([('XBT/EUR', 1)])
        await self.kraken.start()
        await self.wait_connected()
        await sleep(0.1)
        # When
        await self.mock_server.close()
        self.mock_server = MockServer()
        await self.mock_server.start_server("127.0.0.1", self.port)
        # Then
        await sleep(0.2)
        self.assertTrue({
            "event": "subscribe",
            "pair": [
                "XBT/EUR"
            ],
            "subscription": {
                "interval": 1,
                "name": "ohlc"
            }
        } in self.mock_server.msg)

    @asynctest.patch("time.time", mock_time)
    @asynctest.patch("asyncio.sleep", make_me_sleep)
    @asynctest.patch("aio_kraken_ws.stream.logger.exception", asynctest.Mock())
    async def test_exception_from_callback(self):
        """ Exception from callback are not stopping the stream. """
        # Given
        asyncio.sleep.return_value = sleep(0.1)
        self.callback.side_effect = ValueError("callback exception")
        self.kraken.subscribe([('XBT/EUR', 1)])
        await self.kraken.start()
        await self.wait_connected()
        # When
        await self.mock_server.send_a_candle(120, ("XBT/EUR", 1))
        mock_time.return_value = 182
        # Then
        await sleep(1)
        self.logger.exception.assert_called()

    @asynctest.patch("time.time", mock_time)
    @asynctest.patch("asyncio.sleep", make_me_sleep)
    async def test_callback_on_inactive_candle(self):
        """ Callback is trigger even in case of inactivity. """
        self.kraken.subscribe([('XBT/EUR', 1)])
        await self.kraken.start()
        await self.wait_connected()
        # When
        candle = await self.mock_server.send_a_candle(120, ("XBT/EUR", 1))
        await sleep(1)
        mock_time.return_value = 361
        await sleep(1)
        # Then
        self.callback.assert_any_call("XBT/EUR", 1, *candle)
        t, o, h, l, c, v = candle
        self.callback.assert_awaited_with("XBT/EUR", 1, 300, c, c, c, c, 0.0)

    @asynctest.patch("aio_kraken_ws.ws.logger.exception", asynctest.Mock())
    @asynctest.patch("websockets.connect", asynctest.Mock())
    async def test_retry_on_connection_failure(self):
        """ Retry to connect to kraken on erreur. """
        self.kraken.subscribe([('XBT/EUR', 1)])
        await self.kraken.start()
        websockets.connect.return_value = sleep(5.1)
        # When
        with suppress(asyncio.TimeoutError):
            await asyncio.wait_for(self.wait_connected(), timeout=5.1)
        # Then
        logger = logging.getLogger("aio_kraken_ws.ws")
        logger.exception.assert_called_once_with(
            f"Fail to connect to {self.url}... Retry in a second.")

    @asynctest.patch("asyncio.sleep", make_me_sleep)
    @asynctest.patch("aio_kraken_ws.stream.logger.error", asynctest.Mock())
    @asynctest.patch("aio_kraken_ws.stream.logger.warning", asynctest.Mock())
    async def test_manage_errors_interval_not_supported(self):
        """ Kraken send 'Subscription ohlc interval not supported' the dataset is unsubscribed. """
        # Given
        self.kraken.subscribe([('XBT/EUR', 1000)])
        await self.kraken.start()
        await self.wait_connected()
        await sleep(0.11)
        self.assertEqual(self.kraken.datasets, [('XBT/EUR', 1000)])
        # When
        await self.mock_server.send({
            'errorMessage': 'Subscription ohlc interval not supported',
            'event': 'subscriptionStatus',
            'pair': 'XBT/EUR',
            'status': 'error',
            'subscription': {'interval': 1000, 'name': 'ohlc'}
        })
        # Then
        await sleep(0.11)
        self.assertEqual(self.kraken.datasets, [])
        self.logger.error.assert_called_once()
        self.logger.warning.assert_called_once_with('Unsubscribe to (XBT/EUR,1000)')
        # And
        self.kraken.unsubscribe([('XBT/EUR', 1000)])
        await sleep(0.11)
        self.assertEqual(self.kraken.datasets, [])
        self.logger.error.assert_called_once()
