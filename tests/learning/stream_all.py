""" Learning tests to experiment Kraken ws API.

https://www.kraken.com/features/websocket-api


# To stream all kraken datasets and write candles into a file.
python3 -m pytest -vs -W ignore::DeprecationWarning \
    -k TestAioKrakenWs \
    --log-cli-level INFO \
    tests/learning/stream_all.py
"""
import asyncio
from collections import defaultdict
from contextlib import suppress
import json
import logging

import asynctest
import websockets

from aio_kraken_ws import KrakenWs


PAIRS = ["AAVE/AUD", "AAVE/ETH", "AAVE/EUR", "AAVE/GBP", "AAVE/USD", "AAVE/XBT", "ADA/AUD", "ADA/ETH", "ADA/EUR", "ADA/GBP", "ADA/USD", "ADA/USDT", "ADA/XBT", "ALGO/ETH", "ALGO/EUR", "ALGO/GBP", "ALGO/USD", "ALGO/XBT", "ANT/ETH", "ANT/EUR", "ANT/USD", "ANT/XBT", "ATOM/AUD", "ATOM/ETH", "ATOM/EUR", "ATOM/GBP", "ATOM/USD", "ATOM/XBT", "AUD/JPY", "AUD/USD", "BAL/ETH", "BAL/EUR", "BAL/USD", "BAL/XBT", "BAT/ETH", "BAT/EUR", "BAT/USD", "BAT/XBT", "BCH/AUD", "BCH/ETH", "BCH/EUR", "BCH/GBP", "BCH/JPY", "BCH/USD", "BCH/USDT", "BCH/XBT", "COMP/ETH", "COMP/EUR", "COMP/USD", "COMP/XBT", "CRV/ETH", "CRV/EUR", "CRV/USD", "CRV/XBT", "DAI/EUR", "DAI/USD", "DAI/USDT", "DASH/EUR", "DASH/USD", "DASH/XBT", "DOT/AUD", "DOT/ETH", "DOT/EUR", "DOT/GBP", "DOT/USD", "DOT/USDT", "DOT/XBT", "EOS/ETH", "EOS/EUR", "EOS/USD", "EOS/USDT", "EOS/XBT", "ETH2.S", "ETH/AUD", "ETH/CHF", "ETH/DAI", "ETH/USDC", "ETH/USDT", "EUR/AUD", "EUR/CAD", "EUR/CHF", "EUR/GBP", "EUR/JPY", "FIL/AUD", "FIL/ETH", "FIL/EUR", "FIL/GBP", "FIL/USD", "FIL/XBT", "FLOW/ETH", "FLOW/EUR", "FLOW/GBP", "FLOW/USD", "FLOW/XBT", "GNO/ETH", "GNO/EUR", "GNO/USD", "GNO/XBT", "GRT/AUD", "GRT/ETH", "GRT/EUR", "GRT/GBP", "GRT/USD", "GRT/XBT", "ICX/ETH", "ICX/EUR", "ICX/USD", "ICX/XBT", "KAVA/ETH", "KAVA/EUR", "KAVA/USD", "KAVA/XBT", "KEEP/ETH", "KEEP/EUR", "KEEP/USD", "KEEP/XBT", "KNC/ETH", "KNC/EUR", "KNC/USD", "KNC/XBT", "KSM/AUD", "KSM/ETH", "KSM/EUR", "KSM/GBP", "KSM/USD", "KSM/XBT", "LINK/AUD", "LINK/ETH", "LINK/EUR", "LINK/GBP", "LINK/USD", "LINK/USDT", "LINK/XBT", "LSK/ETH", "LSK/EUR", "LSK/USD", "LSK/XBT", "LTC/AUD", "LTC/ETH", "LTC/GBP", "LTC/USDT", "MANA/ETH", "MANA/EUR", "MANA/USD", "MANA/XBT", "NANO/ETH", "NANO/EUR", "NANO/USD", "NANO/XBT", "OMG/ETH", "OMG/EUR", "OMG/USD", "OMG/XBT", "OXT/ETH", "OXT/EUR", "OXT/USD", "OXT/XBT", "PAXG/ETH", "PAXG/EUR", "PAXG/USD", "PAXG/XBT", "QTUM/ETH", "QTUM/EUR", "QTUM/USD", "QTUM/XBT", "REPV2/ETH", "REPV2/EUR", "REPV2/USD", "REPV2/XBT", "SC/ETH", "SC/EUR", "SC/USD", "SC/XBT", "SNX/AUD", "SNX/ETH", "SNX/EUR", "SNX/GBP", "SNX/USD", "SNX/XBT", "STORJ/ETH", "STORJ/EUR", "STORJ/USD", "STORJ/XBT", "TBTC/ETH", "TBTC/EUR", "TBTC/USD", "TBTC/XBT", "TRX/ETH", "TRX/EUR", "TRX/USD", "TRX/XBT", "UNI/ETH", "UNI/EUR", "UNI/USD", "UNI/XBT", "USDC/AUD", "USDC/EUR", "USDC/GBP", "USD/CHF", "USDC/USD", "USDC/USDT", "USDT/AUD", "USDT/CAD", "USDT/CHF", "USDT/EUR", "USDT/GBP", "USDT/JPY", "USDT/USD", "WAVES/ETH", "WAVES/EUR", "WAVES/USD", "WAVES/XBT", "XBT/AUD", "XBT/CHF", "XBT/DAI", "XBT/USDC", "XBT/USDT", "XDG/EUR", "XDG/USD", "ETC/ETH", "ETC/XBT", "ETC/EUR", "ETC/USD", "ETH/XBT", "ETH/CAD", "ETH/EUR", "ETH/GBP", "ETH/JPY", "ETH/USD", "LTC/XBT", "LTC/EUR", "LTC/JPY", "LTC/USD", "MLN/ETH", "MLN/XBT", "MLN/EUR", "MLN/USD", "REP/ETH", "REP/XBT", "REP/EUR", "REP/USD", "XRP/AUD", "XRP/ETH", "XRP/GBP", "XRP/USDT", "XTZ/AUD", "XTZ/ETH", "XTZ/EUR", "XTZ/GBP", "XTZ/USD", "XTZ/XBT", "XBT/CAD", "XBT/EUR", "XBT/GBP", "XBT/JPY", "XBT/USD", "XDG/XBT", "XLM/XBT", "XLM/AUD", "XLM/EUR", "XLM/GBP", "XLM/USD", "XMR/XBT", "XMR/EUR", "XMR/USD", "XRP/XBT", "XRP/CAD", "XRP/EUR", "XRP/JPY", "XRP/USD", "ZEC/XBT", "ZEC/EUR", "ZEC/USD", "YFI/AUD", "YFI/ETH", "YFI/EUR", "YFI/GBP", "YFI/USD", "YFI/XBT", "EUR/USD", "GBP/USD", "USD/CAD", "USD/JPY"]
INTERVALS = [60, 300, 900, 1800, 3600, 14400, 86400]
DATASETS = [(pair, interval // 60) for pair in PAIRS for interval in INTERVALS]

class TestSubscriptionAll(asynctest.TestCase):
    """ Subscription to 1981,  283 pairs x 7 UT. """

    async def setUp(self):
        self.ws = await websockets.connect(f'wss://ws.kraken.com')
        self.messages = defaultdict(list) # message by event
        self.ohlc = defaultdict(list)
        async def listen():
            async for message in self.ws:
                msg = json.loads(message)
                if isinstance(msg, list):
                    # ohlc
                    chan, ohlc, chan_name, pair = msg
                    self.ohlc[(chan, chan_name, pair)].append(ohlc)
                elif isinstance(msg, dict):
                    if "event" in msg:
                        self.messages[msg["event"]].append(msg)
                    else:
                        self.messages["dict"].append(msg)
                else:
                    self.messages["others"].append(msg)
        self.listen_task = asyncio.ensure_future(listen())
        await asyncio.sleep(1)

    async def tearDown(self):
        await self.ws.close()
        await self.listen_task

    async def test_subscribe_to_all(self):
        for interval in INTERVALS:
            await self.ws.send(json.dumps({
                "event": "subscribe",
                "pair": PAIRS,
                "subscription": {
                    "name": "ohlc",
                    "interval": interval // 60
                }
            }))
        await asyncio.sleep(121)

    async def subscribe_to_few(self):
        for interval in INTERVALS[:3]:
            await self.ws.send(json.dumps({
                "event": "subscribe",
                "pair": PAIRS,
                "subscription": {
                    "name": "ohlc",
                    "interval": interval // 60
                }
            }))
        await asyncio.sleep(121)


class TestAioKrakenWs(asynctest.TestCase):

    async def test_using_aio_kraken_ws(self):

        async def callback(pair, interval, timestamp, o, h, l, c, v):
            """ A coroutine handling new candles. """
            with open("candles.txt", "a+") as file:
                row = f"[{pair}:{interval}]({timestamp},{o},{h},{l},{c},{v})\n"
                logging.info(row)
                file.write(row)

        kraken_ws = await KrakenWs.create(callback)
        kraken_ws.subscribe(DATASETS)
        await asyncio.sleep(10*60)
        await kraken_ws.close()
