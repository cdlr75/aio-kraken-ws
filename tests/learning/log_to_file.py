""" Sample script to log candles in a file. """
import asyncio
import logging
from aio_kraken_ws import KrakenWs


async def callback(pair, interval, timestamp, o, h, l, c, v):
    """ A coroutine handling new candles. """
    with open("candles.txt", "a+") as file:
        row = f"[{pair}:{interval}]({timestamp},{o},{h},{l},{c},{v})\n"
        logging.info(row)
        file.write(row)

async def start():
    kraken_ws = await KrakenWs.create(callback)
    kraken_ws.subscribe([('XBT/EUR', 1), ('ETH/EUR', 1)])
    return kraken_ws


def main():
    logging.basicConfig(level="INFO")
    loop = asyncio.get_event_loop()
    loop.run_until_complete(start())
    loop.run_forever()


if __name__ == '__main__':
    main()
