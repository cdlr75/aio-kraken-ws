""" Learning tests to experiment Kraken ws API.

https://www.kraken.com/features/websocket-api
"""
import asyncio
from contextlib import suppress
import time
import json
import aiohttp.test_utils
import websockets
import asynctest


class TestKrakenWsServer(asynctest.TestCase):
    """ Connect to Kraken ws - do basic interaction. """

    async def setUp(self):
        self.ws = await websockets.connect(f'wss://ws.kraken.com')
        self.reply = []

        async def listen():
            async for message in self.ws:
                self.reply.append(json.loads(message))
        self.listen_task = asyncio.ensure_future(listen())
        await asyncio.sleep(1)

    async def tearDown(self):
        await self.ws.close()
        await self.listen_task

    async def test_ping(self):
        """ ping → pong """
        await self.ws.send(json.dumps({
            "event": "ping"
        }))
        await asyncio.sleep(2)  # wait at least one second to get a reply
        self.assertEqual(self.reply[1], {
            "event": "pong"
        })

    async def test_subscribe_to_ohlc(self):
        await self.ws.send(json.dumps({
            "event": "subscribe",
            "pair": ["ADA/CAD", "XTZ/EUR", "XTZ/USD", "XTZ/XBT"],
            "subscription": {
                "name": "ohlc",
                "interval": 1
            }
        }))
        for _ in range(6):
            print(self.reply)
            await asyncio.sleep(10)


class TestGetClosedOhlc(asynctest.TestCase):
    """ Subscribe to ohlc and make ohlc from data. """

    async def test_get_closed_candles_only(self):
        """ Filter ws messages to build and print closed candles only. """
        # redefine listen
        self.listen_task.cancel()
        try:
            await self.listen_task
        except asyncio.CancelError:
            pass

        async def listen():
            candles = {}  # cache per ohlc
            async for message in self.ws:
                msg = json.loads(message)
                if isinstance(msg, dict):
                    continue
                try:
                    sub_id, ohlc, ut, market = msg
                except ValueError:
                    continue

                if (market in candles) and (candles[market]["timestamp"] !=
                                            data["k"]["t"] // 1000):
                pair, interval = self._stream_to_dataset(stream)
                # publish the buffered candle
                await self._notify(self.name, pair, interval, [candles[stream]])
            # update buffer
            candles[stream] = self.ohlc_klass(
                timestamp=int(data["k"]["t"]) // 1000,
                open=Decimal(data["k"]["o"]),
                high=Decimal(data["k"]["h"]),
                low=Decimal(data["k"]["l"]),
                close=Decimal(data["k"]["c"]),
                volume=Decimal(data["k"]["v"]),
            )
        self.listen_task = asyncio.ensure_future(listen())

        await self.ws.send(json.dumps({
            "event": "subscribe",
            "pair": ["ETH/EUR"],
            "subscription": {
                "name": "ohlc",
                "interval": 1
            }
        }))


"""
---
valid reply
[823,
['1568266025.580083',
'1568266080.000000',
'0.00009900',
'0.00009900',
'0.00009900',
'0.00009900',
'0.00009900',
'568.44750717',
1],
'ohlc-1',
'XTZ/XBT']

---
Wrong name
[{'connectionID': 961359034600355779,
  'event': 'systemStatus',
  'status': 'online',
  'version': '0.2.0'},
 {'errorMessage': 'Currency pair not in ISO 4217-A3 format XXBTZEUR',
  'event': 'subscriptionStatus',
  'pair': 'XXBTZEUR',
  'status': 'error',
  'subscription': {'interval': 1, 'name': 'ohlc'}}]
or
[{'connectionID': 11528476018954010555,
  'event': 'systemStatus',
  'status': 'online',
  'version': '0.2.0'},
 {'errorMessage': 'Currency pair not supported XXBT/ZEUR',
  'event': 'subscriptionStatus',
  'pair': 'XXBT/ZEUR',
  'status': 'error',
  'subscription': {'interval': 1, 'name': 'ohlc'}}]
"""
