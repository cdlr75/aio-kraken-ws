import asyncio
import logging
import asynctest
from aio_kraken_ws.kraken_ws import KrakenWs


class TestKrakenWs(asynctest.TestCase):
    """Test KrakenWs class. """

    async def setUp(self):
        self.callback = asynctest.CoroutineMock()
        self.kraken = await KrakenWs.create(self.callback)
        self.logger = logging.getLogger("aio_kraken_ws.stream")

    async def tearDown(self):
        await self.kraken.close()

    async def test_subscribe_to_one_dataset(self):
        """ Simple subscription to one dataset. """
        self.kraken.subscribe([('XBT/EUR', 1)])
        await asyncio.sleep(120)
        self.assertTrue(self.callback.await_count >= 1)

    async def test_subscribe_to_many_datasets(self):
        """ Simple subscription to many dataset. """
        self.kraken.subscribe([
            ('XBT/EUR', 1),
            ('ETH/EUR', 1),
            ('ETH/XBT', 1),
            ('XRP/EUR', 1)])
        await asyncio.sleep(120)
        self.assertTrue(self.callback.await_count >= 4)

    async def test_many_subscriptions(self):
        """ Make several subscriptions. """
        self.kraken.subscribe([('XBT/EUR', 1)])
        self.kraken.subscribe([('ETH/EUR', 1)])
        self.kraken.subscribe([('ETH/XBT', 1)])
        self.kraken.subscribe([('XRP/EUR', 1)])
        await asyncio.sleep(120)
        self.assertTrue(self.callback.await_count >= 4)

    async def test_subscribe_and_unsubscribe_to_one_dataset(self):
        """ Stop a subscription. """
        # Given
        self.kraken.subscribe([('XBT/EUR', 1)])
        await asyncio.sleep(60)
        # When
        self.kraken.unsubscribe([('XBT/EUR', 1)])
        await asyncio.sleep(60)
        # Then
        candles_received = self.callback.await_count
        await asyncio.sleep(60)
        self.assertEqual(self.callback.await_count, candles_received)

    async def test_unsubscribe_to_many_datasets(self):
        """ Stop subscriptions. """
        # Given
        self.kraken.subscribe([
            ('XBT/EUR', 1),
            ('ETH/EUR', 1),
            ('ETH/XBT', 1),
            ('XRP/EUR', 1)])
        await asyncio.sleep(60)
        # When
        self.kraken.unsubscribe([
            ('XBT/EUR', 1),
            ('ETH/EUR', 1),
            ('ETH/XBT', 1),
            ('XRP/EUR', 1)])
        await asyncio.sleep(60)
        # Then
        self.assertFalse(self.kraken.is_connected)

    @asynctest.patch("aio_kraken_ws.stream.logger.error", asynctest.Mock())
    async def test_subscribe_to_unknown_pair(self):
        """ Try to do something weird. """
        await self.kraken.close()
        self.kraken = KrakenWs(self.callback)
        self.kraken.subscribe([('BOT/CRYPTO', 1)])
        await self.kraken.start()
        await asyncio.sleep(5)
        self.logger.error.assert_called_once_with({
            'errorMessage': 'Currency pair not supported BOT/CRYPTO',
            'event': 'subscriptionStatus',
            'pair': 'BOT/CRYPTO',
            'status': 'error',
            'subscription': {'interval': 1, 'name': 'ohlc'}
        })

    async def test_unsubscribe_to_one_dataset(self):
        """ Stop one subscription doesn't stop other streams. """
        # Given
        self.kraken.subscribe([
            ('XBT/USD', 1),
            ('ETH/USD', 1),
            ('ETH/XBT', 1),
            ('XRP/EUR', 1)])
        # When
        self.kraken.unsubscribe([('XRP/EUR', 1)])
        await asyncio.sleep(120)
        # Then
        self.assertTrue(self.callback.await_count >= 3)
