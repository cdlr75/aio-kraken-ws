Changelog
=========

Auto generate from git tags with **changelogfromtags**.

See https://cdlr75.gitlab.io/aio-kraken-ws/CHANGELOG.html
