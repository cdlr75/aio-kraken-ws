.. _GitLab: https://gitlab.com/cdlr75/aio-kraken-ws/

========================
Welcome to aio-kraken-ws
========================

❓ A module to collect ohlc candles from Kraken using WebSockets that is asyncio friendly!

💸 Looking for automated trading tools?
`Botcrypto <https://botcrypto.io>`_ may interest you.

📍 Source code on GitLab_

Key features
============

- Subscribe to kraken data using a single WebSocket.
- Trigger a callback that is coroutine on each new closed candles from kraken.
- Easy subscription/unsubscription to datasets, i.e. [(PAIR, Unit Time)].
- Callback is regulary triggered at each end of the UT intervals, whatever is the number of data received by kraken.


Getting started
===============

Install
-------

.. code-block::

  pip install aio-kraken-ws

Usage
-----

.. code-block:: python

  from aio_kraken_ws import KrakenWs

  # check tests/learning/log_to_file.py for a complete example
  async def callback(pair, interval, timestamp, o, h, l, c, v):
      """ A coroutine handling new candles.

      :param str pair:
      :param int interval: time in minutes
      :param int timestamp: candle open timestamp.
      :param float o: open price
      :param float h: high price
      :param float l: low price
      :param float c: close price
      :param float v: volume
      """
      with open("candles.txt", "a+") as file:
          file.write(f"[{pair}:{interval}]({timestamp},{o},{h},{l},{c},{v})\n")

  kraken_ws = await KrakenWs.create(callback)
  # subscribe to some datasets
  kraken_ws.subscribe([("XBT/EUR", 1), ("ETH/EUR", 5)])
  # get datasets streamed
  print(kraken_ws.datasets)


The `callback` function is called for each dataset at the end of each dataset's unit time interval.

E.g. if subscription start at 4h42.05 to the dataset `("XBT/EUR", 1)`, then callback is triggered at 4h43.00, at 4h44.00, at 4h45.00, etc... For `("XBT/EUR", 60)`, it would be at 5h00.00, at 6h00.00, etc... It's possible to get at most 10ms delay between the exact UT interval ending and the actual datetime of the call.

If **no** new data were received from Kraken during an interval, the callback is triggered with the latest known close price and v=0, as it's described in the following example.

.. code-block:: python

  kraken_ws.subscribe([("XBT/EUR", 1)])
  # time.time() = 120
  await callback("XBT/EUR", 1, 60, 42.0, 57.0, 19.0, 24.0, 150.0)
  # time.time() = 180
  await callback("XBT/EUR", 1, 120, 19.0, 24.0, 8.0, 10.0, 13.0)
  # time.time() = 240 : no data received in 60s, i.e. no activity
  await callback("XBT/EUR", 1, 180, 10.0, 10.0, 10.0, 10.0, 0.0)

An exception raised by the `callback` will be logged and it wont stop the streams.

Dependencies
============

- Python 3.6+
- websockets==8.0.2


Contributing
============

🙏 Please feel free to file an issue on the `bug tracker <https://gitlab.com/cdlr75/aio-kraken-ws/issues>`_ if you have found a bug
or have some suggestion in order to improve the library.

🙏 Please feel free to fork the project from GitLab_ and make a Pull Request.


Authors and License
===================

The ``aio-kraken-ws`` package is written mostly by Constantin De La Roche.

It's *MIT* licensed and freely available.

Feel free to improve this package and send a pull request to GitLab_.


Table Of Contents
=================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   aio_kraken_ws
   CHANGELOG.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
